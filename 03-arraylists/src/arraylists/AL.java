// Written by Nick Hirzel, 2019

package arraylists;

import java.util.ArrayList; // Need to import this to use ArrayLists
import java.util.Collections; // Needed to import for sorting

// AL is here so we don't get confused with the actual ArrayList class
public class AL
{
    public static void main(String[] args)
    {
        /* ArrayLists are very similar to arrays, with the key exception being
         * that ArrayLists can have a size that changes over time. Elements can
         * be added or removed on a whim. Additionally, the syntax for
         * accessing and changing elements will be different from arrays, but
         * functionally it is identical. As a class, ArrayLists also have 
         * additional functionality, such as being able to clear all elements.
         * 
         * ArrayLists are very useful for any situation where the number of 
         * items will not be known ahead of time, like when interacting with
         * a user.
         * 
         * Like other classes, the first step to using ArrayLists is importing
         * its package, `java.util.ArrayList`, which you can see we've done at
         * the top of this file already.
         * 
         * ArrayList is a `generic` class, which means that its type is defined
         * by the programmer. In Java, generics don't support primitive types 
         * like int, boolean, or float. Instead you have to use the wrapper 
         * object associated with each primitive. For example:
         *      boolean             ->  Boolean 
         *      int                 ->  Integer
         *      floats & doubles    ->  Double
         *      char                ->  Character
        */
        
        
        /* ~~~~~~~~~~~~~~ declaration & initialization ~~~~~~~~~~~~~~ */
        // In this case, we've declared an ArrayList that uses a `String` type
        ArrayList<String> names;
        
        // Initializing the variable is similar to what we do for arrays.
        // Unfortunately, in Java 8 there is no way to initialize ArrayLists
        // with values.
        names = new ArrayList<String>();
        
        
        /* ~~~~~~~~~~~~~~ adding new elements ~~~~~~~~~~~~~~ */
        names.add("Ellen Ripley");
        names.add("Darth Vader");
        names.add("Jean-Luc Picard");
        
        System.out.printf("names: %s\n", names);
        
        
        /* ~~~~~~~~~~~~~~ accessing and changing elements ~~~~~~~~~~~~~~ */
        // Here we're accessing element at index 1
        String get_name = names.get(1);
        System.out.printf("They're staring at %s!\n", get_name);
        
        // Changing a specific element
        // In this scenario, "Darth Vader" is being turned into "Xenomorph"
        names.set(1, "Xenomorph");
        System.out.printf("names: %s\n", names);
        
        // Removing a specific element, remove will also return the element
        // that was removed from the ArrayList
        String removed = names.remove(2);
        System.out.printf("The %s ate %s!\n", names.get(1), removed);
        System.out.printf("names: %s\n", names);
        
        
        /* ~~~~~~~~~~~~~~ using ArrayList with loops ~~~~~~~~~~~~~~ */
        // You can use loops in the same way as with array. Instead of `length`
        // you use `size()`
        for(int i = 0; i < names.size(); i++)
        {
            System.out.printf("%s screams!\n", names.get(i));
        }
        
        // Additionally, ArrayList is another object you can use for-each with
        for(String index : names)
        {
            System.out.printf("%s runs away!\n", index);
        }
        
        // You can clear an ArrayList of all elements like this
        names.clear();
        System.out.printf("names: %s\n", names);
        
        
        /* ~~~~~~~~~~~~~~ sorting ArrayLists ~~~~~~~~~~~~~~ */
        // You can sort ArrayList alphanumerically using the
        // Collections class, which is in `java.util.Collections`
        names.add("David");
        names.add("Ashley");
        names.add("Bobby");
        names.add("Tim");
        names.add("Madison");
        System.out.printf("unsorted names: %s\n", names);
        
        Collections.sort(names);
        System.out.printf("sorted names: %s\n", names);
    }
}
