// Written by Nick Hirzel, 2019

package conditionals;

public class Conditionals
{
    @SuppressWarnings("unused") // added because we have purposefully dead code
    
    public static void main(String[] args)
    {
        /* 
         * A conditional statement is one of the most fundamental aspects of
         * programming. With it, we can perform comparisons between values and
         * make branching processes. Without them, a program would just run in
         * a straight line, always performing the same tasks.
         * 
         * Conditionals work by evaluating the expression between the
         * parenthesis, and if the expression is true, the code in brackets
         * will execute.
        */
        
        
        /* ~~~~~~~~~~~~~~ if statements ~~~~~~~~~~~~~~ */
        
        // The basic type of conditional is an if-statement
        // Because the expression is "true", this example will always execute
        {
            if(true)
            {
                System.out.printf("hello!\n");
            }
        }
        
        {
            int num = 10;
            
            // Simple comparison, statement is false so the example will never execute
            if(num < 5)
            {
                System.out.printf("oh no, 10 is less than 5!\n");
            }
            
            // If statements can be nested
            if(num > 9)
            {
                System.out.printf("10 is greater than 9\n");
                if(num < 11)
                {
                    System.out.printf("10 is less than 11\n");
                }
            }
            
            // You can also chain comparisons together
            // && is AND
            // || is OR
            // This example is equivalent to the example above
            if(num > 9 && num < 11)
            {
                System.out.printf("10 is greater than 9 and less than 11\n");
            }
            
            if((num > 4 && num < 5) || num > 9)
            {
                System.out.printf("num is greater than 4 and less than 5 or is greater than 9\n");
            }
            
            // De Morgan's Law applies as well, the two examples below are equivalent
            if(!(num < 9 && num > 11))
            {
                System.out.printf("De Morgan's law check 1\n");
            }
            
            if(!(num < 9) || !(num > 11))
            {
                System.out.printf("De Morgan's law check 2\n");
            }
        }
        
        // Conditionals can evaluate boolean values as well
        {
            boolean value = true;
            if(value)
            {
                System.out.printf("value is true!\n");
            }
        }
            
        // You can store the result of a comparison in a boolean
        {
            int num = 5;
            boolean value = (num > 1);
            
            if(value)
            {
                System.out.printf("num is greater than 1\n");
            }
        }
        
        
        /* ~~~~~~~~~~~~~~ if-else / else-if statements ~~~~~~~~~~~~~~ */
        
        // If-else can be used to execute a block of code if the expression is false
        {
            int num = 10;
            
            if(num < 5)
            {
                System.out.printf("oh no, 10 is less than 5!\n");
            }else
            {
                System.out.printf("turns out 10 is greater than 5\n");
            }
        }
        
        // Else-if can be used to chain together multiple conditional statements
        {
            int num = 7;
            
            if(num > 8)
            {
                System.out.printf("else-if 1 was true, num > 8\n");
            }else if(num > 7)
            {
                System.out.printf("else-if 2 was true, num > 7\n");
            }else if(num > 6)
            {
                System.out.printf("else-if 3 was true, num > 6\n");
            }else if(num > 5)
            {
                System.out.printf("else-if 4 was true, num > 5\n");
            }else if(num > 4)
            {
                System.out.printf("else-if 5 was true, num > 4\n");
            }
        }
        
        
        /* ~~~~~~~~~~~~~~ switch statements ~~~~~~~~~~~~~~ */
        
        // Switch statements are like condensed else-if chains
        {
            int num = 5;
            
            switch(num)
            {
            case 3:
                System.out.printf("case 3 executed\n");
                break; // This breaks out of the switch statement
            case 4:
                System.out.printf("case 4 executed\n");
                break;
            case 5:
                System.out.printf("case 5 executed\n");
                break;
            default: // this is the case that runs if no matches are found
                System.out.printf("no cases matched\n");
                break;
            }
            
            // You can also use switch statements to match strings
            String word = "pizza";
            
            switch(word)
            {
            case "pizza":
                System.out.printf("pizza located\n");
                break;
            case "spaghetti":
                System.out.printf("spaghetti en route\n");
                break;
            case "brownies":
                System.out.printf("brownies coming in\n");
                break;
            default:
                System.out.printf("no food nearby\n");
                break;
            }
        }
    }
}
