// Written by Nick Hirzel, 2019

package arrays;

public class Arrays
{
    @SuppressWarnings("unused") // We have purposefully unused code here
    
    public static void main(String[] args)
    {
        /* Arrays are a convenient way to store contiguous data that is all in
         * a standard format. Arrays can be n-dimensional and be of any type.
         * All of the following examples here use int, but they could just as
         * easily be String, boolean, or a user-defined class.
        */
        
        /* ~~~~~~~~~~~~~~ array declaration ~~~~~~~~~~~~~~ */
        // The two following declarations are equivalent, its purely up to
        // personal preference which to use
        int[] array1;
        int array2[];
    
        
        /* ~~~~~~~~~~~~~~ array initialization ~~~~~~~~~~~~~~ */
        // After you've declared your variable, you need to initialize the
        // array. This allocates the actual array object. Before
        // initialization, the array variable doesn't hold anything.
        
        // Here we're creating an empty array with 10 slots for integers
        array1 = new int[10];
        
        // You can also more conveniently combine the declaration and 
        // initialization into one step
        int[] array3 = new int[10];
        
        // You can initialize an array with values, called an "array literal"
        // When we initialize like this, we don't need to specify the size
        int[] array = new int[] {4, 8, 15, 16, 23, 42};
        
        // array.length returns the number of indices in array
        int size = array.length;
        System.out.printf("There are %d elements in `array`\n", size);
        
        // Setting the elements of an array manually is trivial, but its 
        // important to remember arrays are "zero-indexed" in Java, which means
        // the first element is element 0
        array[0] = 100;
        
        
        /* ~~~~~~~~~~~~~~ using arrays with loops ~~~~~~~~~~~~~~ */
        // Arrays and loops work very naturally together. In the following
        // example we're using a loop to sum the value of every element in
        // the array. 
        int sum = 0;
        for(int i = 0; i < array.length; i++)
        {
            sum += array[i];
        }
        
        System.out.printf("Sum of `array` is %d\n", sum);
        
        // There's a special type of for loop that you can use when dealing
        // with containers, called a for-each loop
        // The iterator must be of the same type as the elements in the array
        for(int i : array)
        {
            System.out.printf("for-each: %d\n", i);
        }
        
        
        /* ~~~~~~~~~~~~~~ multidimensional arrays ~~~~~~~~~~~~~~ */
        // Multidimensional arrays are arrays that have multiple dimensions.
        // For example, you could use a 3-dimensional array to hold block
        // data in a minecraft-esque game.
        // In this example, the array is 500 wide on both sides, and is 40 levels deep
        int[][][] level_data = new int[500][500][40];
        
        // Here we use a for-loop to initialize all 10,000,000 elements to 1
        System.out.printf("Setting 10,000,000 elements...\n");
        for(int z = 0; z < 40; z++)
        {
            for(int y = 0; y < 500; y++)
            {
                for(int x = 0; x < 500; x++)
                {
                    level_data[x][y][z] = 1;
                }
            }
        }
        System.out.printf("Finished\n");
    }
}
