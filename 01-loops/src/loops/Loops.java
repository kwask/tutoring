// Written by Nick Hirzel, 2019

package loops;

public class Loops
{
    public static void main(String[] args)
    {
        /*
         * Loops are a way to repeat a block of code multiple times. Sometimes
         * the number of iterations to repeat will be known ahead of time, but
         * sometimes you'll want it to continue to repeat until a condition has
         * been met. Loops will continue to run as long as the expression
         * that you provided is true.
         * 
         * In Java, there are three ways to write loops:
         *      for: This is the type of loop most often used. It uses an
         *          iterator that tracks the number of times the loop has run
         *          and typically has an end condition that specifies a maximum
         *          number of iterations.
         *          
         *      while: This type of loop is typically used when the number of
         *          iterations is not known, but an end condition is known.
         *      
         *      do-while: This is the rarest type of loop, and one that I've
         *          honestly have never used. This is like a while loop but
         *          it is always guaranteed to run at least once.
         * 
         * Another important tool you can use with loops is `break`. This
         * will immediately exit the loop. In general its always possible
         * to write your loop in a way to avoid using breaks, but a lot of
         * times it's an easy  way to avoid unnecessary work.
         */
        
        
        /* ~~~~~~~~~~~~~~ for loops ~~~~~~~~~~~~~~ */
        
        /* This syntax can be a little hard to read at first
         * for(a; b; c)
         * Statement `a` is executed once when the loop is created, and this is
         *      where the iterator is defined.
         * Statement `b` is the condition expression, as long as the expression
         *      is true the loop will run.
         * Statement `c` is executed every time after the code block is run,
         *      and this is typically where the iterator is increased.
        */
        {
            // This loop will run three times
            for(int i = 0; i < 3; i++)
            {
                System.out.printf("i = %d\n", i);
            }
            
            // This loop will run forever
            for(int i = 0; i < 1; )
            {
                System.out.printf("hello from the infinite loop!\n", i);
                break; // This will immediately exit the loop
            }
            
            // And this loop will never run
            for(int i = 5; i < 3; i++)
            {
                System.out.printf("i = %d\n", i);
            }
            
            // This loop uses a previously defined variable as the iterator
            // Notice that the first statement is absent
            int outside_i = 0;
            for(; outside_i < 3; outside_i++)
            {
                System.out.printf("outside_i: %d\n", outside_i);
            }
        }
        
        /* ~~~~~~~~~~~~~~ while loops ~~~~~~~~~~~~~~ */

        /* The syntax for while loops is much simpler.
         * while(a)
         * Statement `a` is simply an expression, as long as the expression is
         *      true, the loop will continue to run.
        */
        {
            // This loop will run forever
            while(true)
            {
                break; // This will immediately exit the loop
            }
            
            // This loop will run 4 times
            int num = 5;
            while(num > 1)
            {
                num--;
            }
            
            // In real-world applications, there will be a "main loop" that
            // uses a boolean to control when the program will end
            boolean should_quit = false;
            
            while(!should_quit)
            {
                should_quit = true;
            }
        }


        /* ~~~~~~~~~~~~~~ do-while loops ~~~~~~~~~~~~~~ */
        
        /* The syntax for do-while is the same as the while loop, but the 
         * expression is now at the end of the code block. 
        */
        {
            // This do-while will run exactly once then stop
            do
            {
                System.out.printf("doing once!");
            }while(false);
        }
    }
}
